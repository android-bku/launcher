package uni.bku;

import android.graphics.drawable.Drawable;

public class AppItem {
  private Drawable appIcon;
  private String appName;
  private String intentDir;
  private String version;

  public AppItem(Drawable appIcon, String appName, String intentDir, String version) {
    this.appIcon = appIcon;
    this.appName = appName;
    this.intentDir = intentDir;
    this.version = version;
  }

  public Drawable getAppIcon() {
    return appIcon;
  }

  public String getAppName() {
    return appName;
  }

  public String getIntentDir() {
    return intentDir;
  }

  public void setAppIcon(Drawable appIcon) {
    this.appIcon = appIcon;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public void setIntentDir(String intentDir) {
    this.intentDir = intentDir;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
