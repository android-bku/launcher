/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uni.bku.fragment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import uni.bku.AppItem;
import uni.bku.MainActivity;
import uni.bku.R;
import uni.bku.helper.OnStartDragListener;
import uni.bku.helper.SimpleItemTouchHelperCallback;

public class RecyclerGridFragment extends Fragment implements OnStartDragListener {

  private ItemTouchHelper mItemTouchHelper;
  private Activity baseContext = MainActivity.getMainContext();

  public RecyclerGridFragment() {

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    return new RecyclerView(container.getContext());
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    List<AppItem> apps = new ArrayList<>();
    try {
      apps = getApps();
    } catch (NameNotFoundException e) {
      e.printStackTrace();
    }

    final RecyclerListAdapter adapter = new RecyclerListAdapter(getActivity(), this,
        apps);

    RecyclerView recyclerView = (RecyclerView) view;
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(adapter);

    final int spanCount = getResources().getInteger(R.integer.grid_columns);
    final GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), spanCount);
    recyclerView.setLayoutManager(layoutManager);

    ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
    mItemTouchHelper = new ItemTouchHelper(callback);
    mItemTouchHelper.attachToRecyclerView(recyclerView);
  }

  @Override
  public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
    mItemTouchHelper.startDrag(viewHolder);
  }

  public List<AppItem> getApps() throws NameNotFoundException {
    PackageManager packageManager = baseContext.getPackageManager();
    List<ApplicationInfo> applist = packageManager.getInstalledApplications(0);
    List<AppItem> apps = new ArrayList<>();

    for (ApplicationInfo app : applist) {
      //checks for flags; if flagged, check if updated system app
      if ((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
        //installedApps.add(app);
        //it's a system app, not interested
      } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {

      } else {
        String appName = (String) packageManager.getApplicationLabel(app);

        // Get directory of the app, remove the prefix "/data/user/0/"
        String intentDir = app.dataDir.substring(13);

        PackageInfo packageInfo = packageManager.getPackageInfo(app.packageName, 0);

        //packageInfo.versionName, packageInfo.versionCode
        Drawable appIcon = getIcon(app, packageManager);

        String version = packageInfo.versionName;

        apps.add(new AppItem(appIcon, appName, intentDir, version));
      }
    }
    return apps;
  }

  private static Drawable getIcon(ApplicationInfo app, PackageManager packageManager)
      throws NameNotFoundException {
    // Get the application's resources
    Resources res = packageManager.getResourcesForApplication(app);

    // Get a copy of the configuration, and set it to the desired resolution
    Configuration config = res.getConfiguration();
    Configuration originalConfig = new Configuration(config);
    config.densityDpi = DisplayMetrics.DENSITY_XHIGH;

    // Update the configuration with the desired resolution
    DisplayMetrics dm = res.getDisplayMetrics();
    res.updateConfiguration(config, dm);

    // Get the app icon
    return res.getDrawable(app.icon);
  }

  public static Drawable getActivityIcon(Context context, String packageName, String activityName) {
    PackageManager pm = context.getPackageManager();
    Intent intent = new Intent();
    intent.setComponent(new ComponentName(packageName, activityName));
    ResolveInfo resolveInfo = pm.resolveActivity(intent, 0);

    return resolveInfo.loadIcon(pm);
  }
}
