/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uni.bku.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;
import uni.bku.AppItem;
import uni.bku.R;
import uni.bku.helper.ItemTouchHelperAdapter;
import uni.bku.helper.ItemTouchHelperViewHolder;
import uni.bku.helper.OnStartDragListener;

/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
    implements ItemTouchHelperAdapter {

  private final List<AppItem> mItems;

  private final Activity mContext;

  private final OnStartDragListener mDragStartListener;

  PopupWindow popup;

  public RecyclerListAdapter(Activity context, OnStartDragListener dragStartListener,
      List<AppItem> items) {
    mDragStartListener = dragStartListener;
    mItems = items;
    mContext = context;
  }

  @Override
  public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_item, parent, false);
    ItemViewHolder itemViewHolder = new ItemViewHolder(view);
    return itemViewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {

    final AppItem appItem = mItems.get(position);

    holder.appName.setText(appItem.getAppName());
    holder.appVer.setText(appItem.getVersion());
    holder.appIcon.setImageDrawable(appItem.getAppIcon());

    holder.appIcon.setOnClickListener(
        v -> launchAppFromPackageName(appItem.getIntentDir())
    );


    final int finalPosition = position;
    holder.appIcon.setOnLongClickListener(v -> {
      Point location = getPosition(v);
      showDeletePopup(mContext, location, finalPosition);

      mDragStartListener.onStartDrag(holder);
      return true;
    });

    // Start a drag whenever the handle view it touched
//    holder.appIcon.setOnTouchListener(new View.OnTouchListener() {
//      @Override
//      public boolean onTouch(View v, MotionEvent event) {
//        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
//          mDragStartListener.onStartDrag(holder);
//        }
//        return false;
//      }
//    });
  }

  @Override
  public void onItemDismiss(int position) {
    mItems.remove(position);
    notifyItemRemoved(position);
    notifyItemRangeChanged(position, mItems.size());
  }

  @Override
  public void onItemMoved(int fromPosition, int toPosition) {
    Collections.swap(mItems, fromPosition, toPosition);
    notifyItemMoved(fromPosition, toPosition);
    notifyItemRangeChanged(fromPosition < toPosition ? fromPosition : toPosition, mItems.size());
  }

  @Override
  public boolean onItemMove(int fromPosition, int toPosition) {
    popup.dismiss();
    return true;
  }

  @Override
  public int getItemCount() {
    return mItems.size();
  }

  /**
   * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
   * "handle" view that initiates a drag event when touched.
   */
  public static class ItemViewHolder extends RecyclerView.ViewHolder implements
      ItemTouchHelperViewHolder {

    public final TextView appName;
    public final TextView appVer;
    public final ImageView appIcon;

    public ItemViewHolder(View itemView) {
      super(itemView);
      appName = (TextView) itemView.findViewById(R.id.appName);
      appVer = (TextView) itemView.findViewById(R.id.appVer);
      appIcon = (ImageView) itemView.findViewById(R.id.appIcon);
    }

    @Override
    public void onItemSelected() {
      itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
      itemView.setBackgroundColor(0);
    }
  }

  public void launchAppFromPackageName(String intentDir) {
    //Launch an application from package name
    Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(intentDir);
    if (launchIntent != null) {
      mContext.startActivity(launchIntent);
    } else {
      Log.d("----", "Launch intent is null: " + intentDir);
    }
  }

  public boolean removeApp(final int position) {
    Log.d("----", "");
    // Use the Builder class for convenient dialog construction
    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
    builder.setMessage(R.string.dialog_delete_message)
        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            // FIRE ZE MISSILES!
            onItemDismiss(position);
            Log.d("----Removed", position + " - list size" + mItems.size());
          }
        })
        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            // User cancelled the dialog
          }
        });
    // Create the AlertDialog object and return it
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
    return true;

  }

  public Point getPosition(View v) {
    int[] location = new int[2];
    v.getLocationOnScreen(location);
    Log.d("---location", location[0] + ":" + location[1]);
    return new Point(location[0], location[1]);
  }

  // The method that displays the popup.
  private void showDeletePopup(final Activity context, Point p, int position) {
    // Inflate the popup_layout.xml

    View layout = context.getLayoutInflater().inflate(R.layout.delete_popup, null);

//    LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
//    LayoutInflater layoutInflater = (LayoutInflater) context
//        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    View layout = layoutInflater.inflate(R.layout.delete_popup, viewGroup);

    // Creating the PopupWindow
    popup = new PopupWindow(layout,
        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    popup.setFocusable(true);

    // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
    int OFFSET_X = 100;
    int OFFSET_Y = 0;

    // Clear the default translucent background
    popup.setBackgroundDrawable(new BitmapDrawable());

    // Displaying the popup at the specified location, + offsets.
    popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

    // Getting a reference to Close button, and close the popup when clicked.
    Button close = (Button) layout.findViewById(R.id.close);
    close.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        removeApp(position);
        popup.dismiss();
      }
    });
  }
}
