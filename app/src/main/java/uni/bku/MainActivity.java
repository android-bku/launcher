package uni.bku;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import uni.bku.fragment.RecyclerGridFragment;

/**
 * @author Paul Burke (ipaulpro)
 */
public class MainActivity extends AppCompatActivity {

  public static Activity mainContext;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);

    mainContext = this;

    if (savedInstanceState == null) {
      RecyclerGridFragment fragment = new RecyclerGridFragment();
      getSupportFragmentManager().beginTransaction()
          .add(R.id.content, fragment)
          .commit();
    }
  }

  public static Activity getMainContext() {
    return MainActivity.mainContext;
  }

}
